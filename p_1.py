# 综合训练题： 
# 1. 用Numpy, 或者别的办法打开下面的csv文件 。
# 2. 文件是真实的观测数据，数据给出了每天的太阳黑子数，请仔细看字段说明。
# 3. 讨论黑子变化的周期性。
# 交作业要求： 
# 1. notebook格式
# 2. 需要把读数，数据清洗整理、周期计算 分成几个部分。图文并茂。
# 提示： 可以整理成每年的黑子数，然后按年来找周期。不过大家自己来，万一发现了以前从来没有过的周期呢？ 真发现了可以发Nature

import numpy as np
import matplotlib.pyplot as plt
import csv
from scipy import fftpack

file = r'D:\Git_CLASS\week-7\sunspot_data.csv'
with open(file, encoding= 'utf-8' ) as f:
    data = np.loadtxt(f, delimiter=",", skiprows=1)

#sunpots statistic every year
yearsun = np.array([[1818,0]])
year_record01 = np.array([])
l = 0
for i in range(len(data)):
    if data[i][7] == 0:
        continue
    while data[i][1] == data[i-1][1] + 1.0:
        yearsun[l][0] = data[i-1][1]
        yearsun1 = np.array([[data[i][1],data[i][5]]])
        yearsun = np.row_stack((yearsun,yearsun1))
        l += 1
        break
    yearsun[l][1] += data[i][5]
    year_record01 = np.append(year_record01,data[i])
#year-sunpots diagram
year = yearsun[:,0]
sunpots = yearsun[:,1]
plt.xlabel("year")
plt.ylabel("sunpots")
plt.subplot(2,1,1)
plt.plot(year,sunpots)
#find periodicity & periodic diagram
ft_sunpots = fftpack.fft(sunpots, axis=0)
frequencies = fftpack.fftfreq(year.shape[0], 1)
periods = 1/frequencies

plt.subplot(2,1,2)
plt.plot(periods,abs(ft_sunpots) * 1e-3, '*')
plt.xlabel("period")
plt.ylabel("power ($\cdot10^3$)")
plt.xlim(0,30)
plt.show()



